package com.gmail.eapbox.repository.model;

public class User {
        private String login;
        private String password;
        private String name;
        private String role;

        public User() {

        }

        private User(Builder builder) {
                this.login = builder.login;
                this.password = builder.password;
                this.name = builder.name;
                this.role = builder.role;
        }

        public static Builder newBuilder() {
                return new Builder();
        }

        public void setLogin(String login) {
                this.login = login;
        }

        public void setPassword(String password) {
                this.password = password;
        }

        public void setName(String name) {
                this.name = name;
        }

        public void setRole(String role) {
                this.role = role;
        }

        public String getLogin() {
                return login;
        }

        public String getPassword() {
                return password;
        }

        public String getName() {
                return name;
        }

        public String getRole() {
                return role;
        }

        public static final class Builder {
                private String login;
                private String password;
                private String name;
                private String role;

                private Builder() {
                }

                public User build() {
                        return new User(this);
                }

                public Builder login(String login) {
                        this.login = login;
                        return this;
                }

                public Builder password(String password) {
                        this.password = password;
                        return this;
                }

                public Builder name(String name) {
                        this.name = name;
                        return this;
                }

                public Builder role(String role) {
                        this.role = role;
                        return this;
                }
        }
}
