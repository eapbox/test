package com.gmail.eapbox.repository;

import com.gmail.eapbox.repository.model.User;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class UserRepository {
        private static final Logger logger = Logger.getLogger(UserRepository.class);
        private static UserRepository instance = null;

        private UserRepository() {

        }

        public static synchronized UserRepository getInstance() {
                if (instance == null) {
                        instance = new UserRepository();
                }
                return instance;
        }

        public List<User> getUsers() {
                List<User> users = new ArrayList<>();
                File file = new File(getClass().getClassLoader().getResource("users.txt").getFile());
                try (
                        BufferedReader b = new BufferedReader(new FileReader(file))
                ) {
                        String line;
                        String[] head = null;
                        int row = 0;
                        while ((line = b.readLine()) != null) {
                                row++;
                                if (row == 1) {
                                        head = line.split("\\|");
                                } else {
                                        int column = 0;
                                        String[] info = line.split(",");
                                        User user = User.newBuilder()
                                                .build();
                                        for (String element : head) {
                                                switch (element) {
                                                        case "LOGIN":
                                                                user.setLogin(info[column]);
                                                                break;
                                                        case "PASSWORD":
                                                                user.setPassword(info[column]);
                                                                break;
                                                        case "NAME":
                                                                user.setName(info[column]);
                                                                break;
                                                        case "ROLE":
                                                                user.setRole(info[column]);
                                                                break;
                                                }
                                                column++;
                                        }
                                        users.add(user);
                                }
                        }
                        return users;
                } catch (IOException e) {
                        logger.error("cant open file: " + e);
                }
                return Collections.emptyList();
        }

        public void print() {
                logger.info("Hello world from UserRepository");
        }
}
