package com.gmail.eapbox.service;

import com.gmail.eapbox.repository.UserRepository;
import com.gmail.eapbox.repository.model.User;
import com.gmail.eapbox.service.model.UserDTO;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class UserService {
        private static final Logger logger = Logger.getLogger(UserService.class);
        private static UserService instance = null;
        private UserRepository userRepository = UserRepository.getInstance();

        private UserService() {

        }

        public static synchronized UserService getInstance() {
                if (instance == null) {
                        instance = new UserService();
                }
                return instance;
        }

        public List<UserDTO> getUsers() {
                List<User> users = userRepository.getUsers();
                List<UserDTO> orderDTOList = new ArrayList<>();
                for (User user : users) {
                        orderDTOList.add(new UserDTO(user));
                }
                return orderDTOList;
        }

        public void print() {
                logger.info("Hello world from UserService");
                UserRepository repository = UserRepository.getInstance();
                repository.print();
        }
}
