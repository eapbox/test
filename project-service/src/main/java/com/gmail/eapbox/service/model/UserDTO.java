package com.gmail.eapbox.service.model;

import com.gmail.eapbox.repository.model.User;

public class UserDTO {
        private String login;
        private String password;
        private String name;
        private String role;

        public UserDTO(User user) {
                this(
                        newBuilder()
                                .login(user.getLogin())
                                .password(user.getPassword())
                                .name(user.getName())
                                .role(user.getRole())
                );
        }

        private UserDTO(Builder builder) {
                this.login = builder.login;
                this.password = builder.password;
                this.name = builder.name;
                this.role = builder.role;
        }

        public static Builder newBuilder() {
                return new Builder();
        }

        public String getLogin() {
                return login;
        }

        public String getPassword() {
                return password;
        }

        public String getName() {
                return name;
        }

        public String getRole() {
                return role;
        }

        public static final class Builder {
                private String login;
                private String password;
                private String name;
                private String role;

                private Builder() {
                }

                public UserDTO build() {
                        return new UserDTO(this);
                }

                public Builder login(String login) {
                        this.login = login;
                        return this;
                }

                public Builder password(String password) {
                        this.password = password;
                        return this;
                }

                public Builder name(String name) {
                        this.name = name;
                        return this;
                }

                public Builder role(String role) {
                        this.role = role;
                        return this;
                }
        }
}
