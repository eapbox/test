package com.gmail.eapbox.web.servlet;

import com.gmail.eapbox.service.UserService;
import com.gmail.eapbox.service.model.UserDTO;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class MainServlet extends HttpServlet {
        private static final Logger logger = Logger.getLogger(MainServlet.class);
        private final UserService userService = UserService.getInstance();

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                logger.info("Hello world from servlet");
                UserService.getInstance().print();

                List<UserDTO> users = userService.getUsers();
                req.setAttribute("users", users);
                req.getRequestDispatcher("/WEB-INF/pages/main.jsp").forward(req, resp);
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        }
}
