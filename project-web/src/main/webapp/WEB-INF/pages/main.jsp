<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>main</title>
</head>
<body>

<table align="center" width="800">
    <tr>
        <th>LOGIN</th>
        <th>PASSWORD</th>
        <th>NAME</th>
        <th>ROLE</th>
    </tr>
    <c:forEach var="item" items="${users}">
        <tr align="center">
            <td><c:out value="${item.login}"/></td>
            <td><c:out value="${item.password}"/></td>
            <td><c:out value="${item.name}"/></td>
            <td><c:out value="${item.role}"/></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
